#!/bin/env python
#-*- encoding: utf-8 -*-


import json
import requests
import time

import uuid
import hashlib
import sys
import re

class WulBotFloawPrint():
    # Author: pai
    # function: for chat with bot by script
    def __init__(self, line_char_size=25, prefix_space_size=6):
        self.line_char_size=line_char_size
        self.prefix_space_size=prefix_space_size

    def bot_msg(self, msgtext):
        self.format_msg(msgtext, False)
    def user_msg(self, msgtext):
        self.format_msg(msgtext, True)

    def format_msg(self, msgtext, is_user=True):
        headline="-"
        print("+-" + headline + self.__get_padding(self.line_char_size, headline, "-") + "-+")
        if is_user:
            headline="用户："
            print("| " + headline + self.__get_padding(self.line_char_size, headline) + " |")
        else:
            headline="：系统"
            print("| " + self.__get_padding(self.line_char_size, headline) + headline + " |")
        for line in self.__split_msgtext(self.line_char_size, msgtext, is_user):
            print("| " + line + " |")
        footline="-"
        print("+-" + footline + self.__get_padding(self.line_char_size, footline, "-") + "-+")
    
    def __count_cn_en(self, msgtext):
        cn_count=(len(msgtext.encode())-len(msgtext))//2
        en_count=len(msgtext)-cn_count
        return cn_count, en_count
    
    def __lengthb(self, msgorlen):
        if isinstance(msgorlen, str):
            cn_count, en_count = self.__count_cn_en(msgorlen)
            return cn_count*2 + en_count - (len(re.findall('[“”‘’]', msgorlen)))
        else:
            return msgorlen*2

    def __get_padding(self, length, msgtext, padding=" "):
        # cn_count, en_count = self.__count_cn_en(msgtext)
        padding_len=self.__lengthb(length) - self.__lengthb(msgtext) # self.__lengthb(cn_count) - en_count
        return padding * padding_len

    def __split_msgtext(self, maxlen, msgtext, is_user=True):
        one_line=""
        msglines=[]
        for msgtext in msgtext.split("\n"):
            for c in msgtext:
                # one_line+=c
                if self.__lengthb(one_line+c) + self.prefix_space_size > self.__lengthb(maxlen):
                    
                    if is_user:
                        one_line=" "*self.prefix_space_size + one_line
                    else:
                        one_line=one_line + " "*self.prefix_space_size
                    one_line=one_line+self.__get_padding(maxlen, one_line)
                    msglines.append(one_line)
                    one_line=c
                else:
                    one_line+=c
            if one_line:
                if is_user:
                    one_line=" "*self.prefix_space_size + one_line
                else:
                    one_line=one_line + " "*self.prefix_space_size
                one_line=one_line+self.__get_padding(maxlen, one_line)
                msglines.append(one_line)
                one_line=""
        return msglines



class Test(object):
    _host = '123.57.173.102:8801'

    # HOST = 'http://101.200.177.95:6124'
    # KEY = 'xOQiN5Bsr2O6vFPgMsyHl96eiU3xmuQO00c78bad9d5589a654'
    # SECRET = 'kMy8m226nnv4exTq5H3U'

    # **正式环境**   
    HOST = 'https://openapi.wul.ai'
    KEY = 'xOQiN5Bsr2O6vFPgMsyHl96eiU3xmuQO00c78bad9d5589a654'
    SECRET = 'kMy8m226nnv4exTq5H3U'

    MSG_THRESHOLD=0.65

    WBFP=WulBotFloawPrint() # 打印box 按照默认参数即可

    @classmethod
    def get_headers(cls):  
        secret = cls.SECRET
        pubkey = cls.KEY    
        #秒级别时间戳  
        timestamp = str(int(time.time()))  
        nonce = "ABCD"  
        sign = hashlib.sha1((nonce + timestamp + secret).encode("utf-8")).hexdigest()
        data = {  
            "pubkey": pubkey,  
            "sign": sign,  
            "nonce": nonce,  
            "timestamp": timestamp  
        }  
        headers = {}  
        for k, v in data.items():  
            headers["Api-Auth-" + k] = v  
        return headers 

    @classmethod
    def create_user(cls,user_id):
        sendobj = {
            "imgurl": '',
            "nickname": user_id,
            "username": user_id,
        }
        url = cls.HOST + '/v1/user/create'
        data=json.dumps(sendobj)
        headers=cls.get_headers()
        r = requests.post(url, data=data, headers=headers)
        # print(r.content)
        return sendobj 


    @classmethod
    def bot_response(cls,user_id,msg):
        url = cls.HOST + '/v1/msg/inner/bot-response'

        sendobj = {
                    "msg_body": {
                            "extra": user_id,
                            "text": {
                                "content": msg,
                                "tts": "",
                                "tts_type": "AMR"
                            },
                        },
                    "user_id": user_id,
                }
        # print url
        # print json.dumps(sendobj,ensure_ascii=False)
        r = requests.post(url, data=json.dumps(sendobj), headers=cls.get_headers())
        # print (r.content.decode()+'\n')
        return r.content.decode()

    @classmethod
    def receive_user_msg(cls, data, user_id):
        """
        接收用户消息
        :param data:
        :param user_id:
        :return:
        """
        data = {
        "msg_body": {
            "text": {
            "content": data,
            "tts": "",
            "tts_type": "AMR"
            },

        },
        "third_msg_id": "",
        "user_id": user_id
        }
        url = cls.HOST + "/v1/msg/receive"
        return cls.send_request(url, data)
    

    @classmethod
    def get_msg_history(cls, user_id, num=10, msg_id=""):
        """
        获取消息历史
        :param user_id:
        :return:
        """
        post_data = {
            "direction": "FORWARD", # FORWARD BACKWARD
            "msg_id": msg_id,
            "num": num,
            "user_id": user_id
            }
        url = cls.HOST + "/v1/msg/history"
        return cls.send_request(url, post_data)

    @classmethod
    def sync_msg(cls, user_id, suggested_response):
        """
        调用消息同步接口，suggested_response为list，调用/v1/msg/sync接口的参数为dict，多条回复时循环调用即可
        """
        url = cls.HOST + '/v1/msg/sync'
        
        for suggested in suggested_response:   
            post_data = {
                "user_id": user_id,
                "msg_body":{
                    "text":{
                        "content":"test123"
                    }
                },
                "msg_ts": int(time.time()*1000),
                "suggested_response": suggested
            }
            return cls.send_request(url, post_data)

    @classmethod
    def send_request(cls, url, data):
        res = requests.post(url,
                data=json.dumps(data),
                headers=cls.get_headers()
                )
        result = ''
        try:
            result = json.loads(res.content)
        except Exception as e:
            raise
        return result

    @classmethod
    def chat(cls, user_id, query_list):
        cls.run_flow(user_id, query_list, model="chat")
    @classmethod
    def check(cls, user_id, query_list):
        cls.run_flow(user_id, query_list, model="check")

    @classmethod
    def run_flow(cls, user_id, query_list, model="chat"):
        print ("===========%d=============" % int(time.time()*1000))
        for query_pair in query_list:
            check_flag=False
            if isinstance(query_pair, str):
                query=query_pair
                check_flag=False
            else:
                query=query_pair[0]
                std_bot_msgs=list(query_pair[1]) if query_pair[1] else []
                check_flag=True

        # for query in query_list:
            result=Test.bot_response(user_id,query)
            # print(result)
            rsps=json.loads(result)["bot_response"]["suggested_response"] # [0]["response"]
            if model == "chat":
                cls.WBFP.user_msg(query)
            # print("-"*100)
            for rsp in rsps:
                source=rsp["source"]
                score = float(rsp["score"])
                if score < cls.MSG_THRESHOLD:
                    continue
                if source == "TASK_BOT":
                    action = rsp["detail"]["task"]["action"]
                    for msgs in rsp["response"]:
                        content = msgs["msg_body"]["text"]["content"]
                        # print ("系统：\t%s\t%s" % (content, action))
                        if model == "chat":
                            cls.WBFP.bot_msg(content)
                        if model != "chat" and check_flag and not (not std_bot_msgs or (re.search(std_bot_msgs.pop(0), content))):
                            print("error: 机器人回复不在预期范围\t{0} ".format(query_list))
                            return False
                    if model != "chat" and check_flag and not (not query_pair[2] or (re.search(query_pair[2], action))):
                        print("error: 返回action不在预期范围\t{0}".format(query_list))
                        return False
                    Test.sync_msg(user_id, suggested_response=json.loads(result)["bot_response"]["suggested_response"])
                    
                elif source == "QA_BOT":
                    for msgs in rsp["response"]:
                        content = msgs["msg_body"]["text"]["content"]
                        # print("系统：\t%s" % content)


def test():
    # user_id = "%d" % time.time()
    user_id="测试: pai"
    Test.create_user(user_id)
    # 同步获取
    # Test.chat(user_id, [("taskb", ("task_1\$\$", ), None), ("张三丰", ("task_2\$\$", ), None), ("yes", ("task_3\$\$", ), None), ("yes", ("task_4\$\$", "task_5\$\$"), None), ("no", ("task_7\$\$", ), None), ("months_of_age_0", ("task_9\$\$", "task_12\$\$"), None), ("no", ("task_13\$\$", ), None), ("no", ("task_17\$\$", ), None), ("yes", ("task_18\$\$", ), None), "taske"])
    # / Test.chat(user_id, ["taskb", "张三丰", "yes", "yes", 'no', "months_of_age_0",  "no", "no", "yes", "taske"])    #task_7的位置出现task_6
    # Test.check(user_id, [("taskb", ("task_1\$\$", ), None), ("张三丰", ("task_2\$\$", ), None), ("yes", ("task_3\$\$", ), None), ("yes", ("task_4\$\$", "task_5\$\$"), None), ("no", ("task_7\$\$", ), None), ("months_of_age_0", ("task_9\$\$", "task_12\$\$"), None), ("no", ("task_13\$\$", ), None), ("yes", ("task_14\$\$", ), None), ("yes", ("task_17\$\$", ), None), ("yes", ("task_18\$\$", ), None), "taske"])
    # / Test.chat(user_id, ["taskb", "张三丰", "yes", "yes", 'no', "months_of_age_0",  "no", "yes", "yes", "taske"])    #task_7的位置出现task_6

    # Test.check(user_id, [("taskb", ("task_1\$\$", ), None), ("张三丰", ("task_2\$\$", ), None), ("no", ("task_5\$\$", ), None), ("no", ("task_7\$\$", ), None), ("months_of_age_0", ("task_9\$\$", "task_12\$\$"), None), ("no", ("task_13\$\$", ), None), ("no", ("task_17\$\$", ), None), ("yes", ("task_18\$\$", ), None), "taske"])
    # Test.chat(user_id, ["taskb", "张三丰", "no", 'no', "months_of_age_0",  "no", "no", "yes", "taske"])    #task_7的位置出现task_6
    # Test.check(user_id, [("taskb", ("task_1\$\$", ), None), ("张三丰", ("task_2\$\$", ), None), ("no", ("task_5\$\$", ), None), ("no", ("task_7\$\$", ), None), ("months_of_age_0", ("task_9\$\$", "task_12\$\$"), None), ("no", ("task_13\$\$", ), None), ("yes", ("task_14\$\$", ), None), ("yes", ("task_17\$\$", ), None), ("yes", ("task_18\$\$", ), None), "taske"])
    Test.chat(user_id, ["刘金艳"])    #task_7的位置出现task_6


if '__main__' == __name__:
    test()


#     string="""你好我是中文，Helle I'm English你好我是中文，Helle I'm English你好我是中文，Helle I'm English你好我是中文"""+\
#     """，Helle I'm English你好我是中文，Helle I'm English你好我是中文!@#$"""+\
#     """
    
# >> 1 添加个回车空几行会怎样
# >> 2 试试看就知道了

# >> 3 好吧
#     """
    
#     print("默认参数 的时候")
#     wbfp=WulBotFloawPrint()
#     wbfp.user_msg(string)
#     wbfp.bot_msg(string)

#     print("\n\n调整参数：line_char_size=%d, prefix_space_size=%d 的时候" % (20, 3))
#     wbfp=WulBotFloawPrint(20, 3)
#     wbfp.user_msg(string)
#     wbfp.bot_msg(string)

#     print("\n\n调整参数：line_char_size=%d, prefix_space_size=%d 的时候" % (40, 15))
#     wbfp=WulBotFloawPrint(40, 15)
#     wbfp.user_msg(string)
#     wbfp.bot_msg(string)

#     print("\n\n调整参数：line_char_size=%d, prefix_space_size=%d 的时候" % (50, 8))
#     wbfp=WulBotFloawPrint(50, 8)
#     wbfp.user_msg(string)
#     wbfp.bot_msg(string)