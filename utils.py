import argparse
import os
import configparser
import redis
import wulai_logger as logger
import json


parser = argparse.ArgumentParser()
BASE_DIR=os.path.dirname(__file__) # 项目根目录

parser.add_argument("-p", "--port", action="store",
                  dest="port",
                  default=9993,
                  type=int,
                  help="端口")


parser.add_argument("-l", "--log_path", action="store", 
                  dest="log_path", 
                  default=os.path.join(BASE_DIR, 'logs'),
                  type=str,
                  help="全局日志路径")

parser.add_argument("-L", "--log_level", action="store", 
                  dest="log_level", 
                  default="INFO",
                  type=str,
                  help="日志等级：使用范围\nFATAL：致命错误\nCRITICAL：特别糟糕的事情，如内存耗尽、磁盘空间为空，一般很少使用\nERROR：发生错误时，如IO操作失败或者连接问题\nWARNING：发生很重要的事件，但是并不是错误时，如用户登录密码错误\nINFO：处理请求或者状态变化等日常事务\nDEBUG：调试过程中使用DEBUG等级，如算法中每个循环的中间状态")

parser.add_argument("-c", "--conf", action="store", 
                  dest="conf", 
                  default="tornado_template.conf",
                  type=str,
                  help="配置文件")


FLAGS = parser.parse_args()

CONFIG=configparser.ConfigParser()
CONFIG.read(FLAGS.conf)


_redis_inst = redis.StrictRedis(
    CONFIG.get("redis", "host"), 
    int(CONFIG.get("redis", "port")), 
    CONFIG.get("redis", "db"), 
    CONFIG.get("redis", "password"))


# 获取一个实体字典的引用，注意，是引用
def get_entity_ref(entities, entity_name):
    try:
        for entity in entities:
            if entity_name == entity["name"]:
                return entity
    except:
        pass
    return None

# 获取一个实体的实体值
def get_entity_value(entities, entity_name):
    try:
        for entity in entities:
            if entity_name == entity["name"]:
                return entity["value"]
    except:
        return None
    return None

# 流程控制，在webhook里面修改跳转方向
def change_flow(one_suggested_response, 
                next_reply, next_action="", 
                cur_entity_name=None, entity_value="", 
                last_entity_name=None,
                kill_task=False):
    """
    这里用于实现非正常流程跳转，也就是在webhook里面通过判断机器人做跳转

    one_suggested_response和next_reply 为必须项
    如果只给出one_suggested_response和next_reply，则相当于只修改话术，不修改流程
    参数说明：
        one_response
            对params["responses"]["suggested_response"] 任一元素的引用
        next_action
            希望下一个跳到哪哥action
        next_reply
            展示给用户的话术
        cur_entity_name
            跳转需要修改的实体名称
        entity_value
            跳转需要修改的实体名称对应的实体值
            默认填空（“”）
    """
    if cur_entity_name or kill_task:
        state=1
    else:
        state=0
    try:
        entities=one_suggested_response["detail"]["task"]["entities"]
        if kill_task:
            kill_task_entities=[]
            kill_task_entities.append(get_entity_ref(entities, "app_id"))
            kill_task_entities.append(get_entity_ref(entities, "task_id"))
            one_suggested_response["detail"]["task"]["entities"]=kill_task_entities
            logger.info("提前终结任务：%s" % json.dumps(one_suggested_response["detail"]["task"]["entities"]))
        elif cur_entity_name:
            one_suggested_response["detail"]["task"]["action"]=next_action
            entity_ref=get_entity_ref(entities, cur_entity_name)
            logger.info("%s关联到的实体：%s" % (cur_entity_name, json.dumps(entity_ref)))
            entity_ref["seg_value"]=entity_value
            entity_ref["value"]=entity_value

        if isinstance(next_reply, str):
            first_answer=one_suggested_response["response"][0]
            first_answer["msg_body"]["text"]["content"]=next_reply
            first_answer["msg_body"]["text"]["content"]=next_reply
        elif isinstance(next_reply, (list, tuple)):
            for idx in range(len(one_suggested_response["response"])):
                one_answer=one_suggested_response["response"][idx]
                one_answer["msg_body"]["text"]["content"]=next_reply[idx]
                one_answer["msg_body"]["text"]["content"]=next_reply[idx]

        if last_entity_name:
            new_entity={"name":"last_entity_name","value":last_entity_name,"seg_value":last_entity_name}
            entities.append(new_entity)
            logger.info("下一个实体应该是：%s" % last_entity_name)
        one_suggested_response["detail"]["task"]["state"]=state
    except:
        logger.error("webhook内部变更出现异常", exc_info=True)
        return False
    return True


def fill_entity(entities, entity_name, entity_value=""):
    """
    用于填充，修改，主动填充实体

    参数说明：
        entities  是一个实体列表的引用
        entity_name     是一个实体名称，如果存在，会修改，不存在会增加
        entity_value    默认为""
    """
    logger.info("\n\nfill_entity\twebhook填充实体")
    entity_ref = get_entity_ref(entities, entity_name)
    if entity_ref:
        logger.info("fill_entity\twebhook填充实体\t%s=%s" %(entity_name, entity_value))
        entity_ref["value"]=entity_value
        entity_ref["seg_value"]=entity_value
    else:
        logger.info("fill_entity\twebhook增加实体\t%s=%s" %(entity_name, entity_value))
        entities.append({"name":entity_name, "value":entity_value, "seg_value":entity_value})