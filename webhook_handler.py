# -*- coding: UTF-8 -*-
import wulai_logger as logger
import json
import redis
from copy import deepcopy
from utils import CONFIG, _redis_inst
import fasttext

cls = fasttext.load_model("model/lvyou.model.bin")

def get_entity_ref(entities, entity_name):
    try:
        for entity in entities:
            if entity_name == entity["name"]:
                return entity
    except:
        pass
    return None

def get_entity_value(entities, entity_name):
    try:
        for entity in entities:
            if entity_name == entity["name"]:
                return entity["value"]
    except:
        return ""
    return ""


def change_flow(one_suggested_response, 
                next_reply, next_action="", 
                entity_name=None, value_for_entity="", 
                last_entity_name=None,
                kill_task=False,
                state=1):
    """
    这里用于实现非正常流程跳转，也就是在webhook里面通过判断机器人做跳转
    参数说明：
        one_response
            对params["responses"]["suggested_response"] 任一元素的引用
        next_action
            希望下一个跳到哪哥action
        next_reply
            展示给用户的话术
        entity_name
            跳转需要修改的实体名称
        value_for_entity
            跳转需要修改的实体名称对应的实体值
            默认填空（“”）
    """
    if not kill_task and (not entity_name):
        logger.info("kill_task=False时entity_name必须是字符串并且在entities列表里面出现")
        return False

    try:
        if isinstance(next_reply, str):
            one_suggested_response["response"]=[one_suggested_response["response"][0]]
            first_answer=one_suggested_response["response"][0]
            first_answer["msg_body"]["text"]["content"]=next_reply
            first_answer["show_content"]=next_reply
        else:
            new_ans=one_suggested_response["response"][0]
            one_suggested_response["response"]=[]
            for one in next_reply:
                new_ans=deepcopy(new_ans)
                new_ans["msg_body"]["text"]["content"]=one
                new_ans["show_content"]=one
                new_ans["delay_ts"]+=1
                
                one_suggested_response["response"].append(new_ans)

        source=one_suggested_response["source"]
        if source != "TASK_BOT":
            return True
            
        entities=one_suggested_response["detail"]["task"]["entities"]
        if kill_task:
            kill_task_entities=[]
            kill_task_entities.append(get_entity_ref(entities, "app_id"))
            kill_task_entities.append(get_entity_ref(entities, "task_id"))
            one_suggested_response["detail"]["task"]["entities"]=kill_task_entities
            logger.info("提前终结任务：%s" % json.dumps(one_suggested_response["detail"]["task"]["entities"]))
        else:
            one_suggested_response["detail"]["task"]["action"]=next_action
            entity_ref=get_entity_ref(entities, entity_name)
            if entity_ref:
                logger.info("%s关联到的实体：%s" % (entity_name, json.dumps(entity_ref)))
                entity_ref["seg_value"]=value_for_entity
                entity_ref["value"]=value_for_entity
            else:
                logger.info("未关联到实体，增加%s=%s" % (entity_name, value_for_entity))
                new_entity={"name":entity_name,"value":value_for_entity,"seg_value":value_for_entity}
                entities.append(new_entity)

        

        if last_entity_name:
            new_entity={"name":"last_entity_name","value":last_entity_name,"seg_value":last_entity_name}
            entities.append(new_entity)
            logger.info("下一个实体应该是：%s" % last_entity_name)
        one_suggested_response["detail"]["task"]["state"]=state
    except:
        logger.error("webhook内部变更出现异常", exc_info=True)
        return False
    return True

def get_reply(params):
    logger.info("get_reply start "+">"*50)
    # 获取基本信息
    user_id=params["user_id"]
    query=params["query"]
    for one_response in params["responses"]["suggested_response"]:
        try:
            source=one_response["source"]
        except:
            source = "DEFAULT_ANSWER_SOURCE"
            logger.info("无法获取任何回复，将进行兜底回复")
        if source == "DEFAULT_ANSWER_SOURCE":
            logger.info("无法召回任何机器人和知识点，将进行兜底回复")
        elif source == "TASK_BOT":
            # 是机器人
            logger.info("召回任务机器人")
            logger.info("get_reply 传入数据：params=%s" % json.dumps(params))
            
            # 获取实体列表引用和action的值
            entities=one_response["detail"]["task"]["entities"]
            action=one_response["detail"]["task"]["action"]

            if action == "askloseweight" and int(get_entity_value(entities, "interval")) > 1:
                # TODO
                cur_entity_name="yesno"
                cur_entity_value=cls.predict([query], 1)[0][0].replace("__label__","")
                
                if cur_entity_value=="是":
                    next_reply="请问你需要什么样子的减肥药？50粒还是20粒还是试用装？"
                    next_action="askkind"
                    last_entity_name="losspillkind"
                    change_flow(one_response, next_reply, 
                            next_action, cur_entity_name, cur_entity_value，state=1)
                else:
                    next_reply="祝你生活你的身体棒棒的呦，请不要多吃呦，期待下次为您服务愉快"
                    change_flow(one_response, next_reply=next_reply, kill_task=True，state=1)
            
            if action == "另外一个需要特殊处理的别名，必须你可能需要访问数据库？读取redis？，你看着办":
                # TODO
                pass
                

        else:
            # 召回问答机器人或者关键词机器人
            logger.info("召回问答机器人或者关键词机器人，将不做处理")

    logger.info("get_reply end "+"<"*50)

    return params["responses"]["suggested_response"]
